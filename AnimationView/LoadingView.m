//
//  LoadingView.m
//  iOSHelper
//
//  Created by Chair on 2020/3/5.
//  Copyright © 2020 Chair. All rights reserved.
//

#import "LoadingView.h"

static CGFloat const kDefault_duration = 2.0f;

static CGFloat const kDefault_repeat_count = MAXFLOAT;

static CGFloat const kDefault_line_width = 2.0f;

static CGFloat const kDefault_view_width = 35.0f;

static CGFloat const kDefault_view_height = 35.0f;

#define kDefault_view_frame CGRectMake(0, 0, kDefault_view_width, kDefault_view_height)

#define kDefault_fill_color [UIColor clearColor].CGColor

#define kDefault_stroke_color [UIColor grayColor].CGColor

@interface LoadingView ()

@property (nonatomic, weak) CAShapeLayer *shapeLayer;

@end

@implementation LoadingView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self initialize];
        [self createSubviews];
    }
    return self;
}

- (instancetype)init {
    if (self = [super init]) {
        [self initialize];
        [self createSubviews];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        [self initialize];
        [self createSubviews];
    }
    return self;
}

+ (instancetype)startAnimationWithStyle:(DefaultStyle)style {
    LoadingView *view = [[self alloc] init];
    view.style = style;
    UIWindow *window = [[[UIApplication sharedApplication] windows] objectAtIndex:0];
    view.center = window.center;
    [window addSubview:view];
    return view;
}

- (void)setStyle:(DefaultStyle)style {
    _style = style;
    _shapeLayer.strokeColor = style == DefaultStyleDark ? [UIColor blackColor].CGColor : [UIColor whiteColor].CGColor;
}

- (void)startAnimation {
    
}


#pragma mark - 数据相关单元
- (void)initialize {
    self.frame = CGRectEqualToRect(self.frame, CGRectZero) ? kDefault_view_frame : self.frame;
    self.backgroundColor = [UIColor clearColor];
}


#pragma mark - 布局相关单元
- (void)createSubviews {
    CABasicAnimation * animation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    animation.fromValue = @(0.0f);
    animation.toValue = @(1.0f);
    animation.duration = _duration > 0 ?: kDefault_duration;
    animation.repeatCount = _repeatCount > 0 ?: kDefault_repeat_count;
    animation.autoreverses = YES;
    animation.removedOnCompletion = YES;
    
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    UIBezierPath * path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds)) radius:shapeLayer.bounds.size.width / 2.0 startAngle:0 endAngle:2 * M_PI clockwise:YES];
    shapeLayer.path = path.CGPath;
    shapeLayer.fillColor = _fillColor ? _fillColor.CGColor : kDefault_fill_color;
    shapeLayer.strokeColor = _strokeColor ? _strokeColor.CGColor : kDefault_stroke_color;
    shapeLayer.lineWidth = _lineWidth > 0 ?: kDefault_line_width;
    [shapeLayer addAnimation:animation forKey:@"strokeEndAniamtion"];
    [self.layer addSublayer:shapeLayer];
    _shapeLayer = shapeLayer;
        
    CABasicAnimation *view_animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    view_animation.toValue = @(2 * M_PI);
    view_animation.duration = animation.duration / 2.0;
    view_animation.repeatCount = animation.repeatCount;
    [self.layer addAnimation:view_animation forKey:@"rotaionAniamtion"];
}

@end
