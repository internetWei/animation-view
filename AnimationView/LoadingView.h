//
//  LoadingView.h
//  iOSHelper
//
//  Created by Chair on 2020/3/5.
//  Copyright © 2020 Chair. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, DefaultStyle) {
    DefaultStyleDark  = 0,
    DefaultStyleLight = 1
};

/// 前快后慢的转圈动画
@interface LoadingView : UIView

@property (nonatomic, assign) CGFloat duration;

@property (nonatomic, assign) CGFloat repeatCount;

@property (nonatomic, strong) UIColor *fillColor;

@property (nonatomic, strong) UIColor *strokeColor;

@property (nonatomic, assign) CGFloat lineWidth;

@property (nonatomic, assign) DefaultStyle style;

//- (instancetype)initWithFrame:(CGRect)frame duration:(CGFloat)duration repeatCount:(NSUInteger)repeatCount strokeColor:(UIColor *)strokeColor backgroundColor:(UIColor *)backgroundColor width:(CGFloat)width;

/** 使用默认参数快速创建 */
+ (instancetype)startAnimationWithStyle:(DefaultStyle)style;

- (void)startAnimation;

@end

NS_ASSUME_NONNULL_END
