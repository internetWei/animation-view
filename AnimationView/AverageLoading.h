//
//  AverageLoading.h
//  iOSHelper
//
//  Created by Chair on 2020/3/5.
//  Copyright © 2020 Chair. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 均匀的等待加载动画
@interface AverageLoading : UIView

/** 背景颜色 */
@property (strong, nonatomic) UIColor *strokeBackgroundColor;
/** 线条颜色 */
@property (strong, nonatomic) UIColor *strokeColor;
/** 宽度 */
@property (assign, nonatomic) CGFloat strokeWidth;
/** 起点位置(0.0~1.0) */
@property (assign, nonatomic) CGFloat strokeStart;
/** 结束位置(0.0~1.0) */
@property (assign, nonatomic) CGFloat strokeEnd;
/** 动画时长 */
@property (assign, nonatomic) CFTimeInterval duration;

/** 开始动画 */
- (void)startAnimation;

/** 停止动画 */
- (void)stopAnimation;

/** 暂停动画 */
- (void)pauseAnimation;

/** 恢复动画 */
- (void)resumeAnimation;

@end

NS_ASSUME_NONNULL_END
