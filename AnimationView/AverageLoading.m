//
//  AverageLoading.m
//  iOSHelper
//
//  Created by Chair on 2020/3/5.
//  Copyright © 2020 Chair. All rights reserved.
//

#import "AverageLoading.h"

@interface AverageLoading ()

/** 动画Layer */
@property (weak, nonatomic) CALayer *animationLayer;
/** 背景Layer */
@property (weak, nonatomic) CALayer *backgroundLayer;
/** 动画 */
@property (weak, nonatomic) CABasicAnimation *rotationAnimation;
/** 播放状态 */
@property (assign, nonatomic) bool isPause;

@end

@implementation AverageLoading

- (void)startAnimation {
    _isPause = false;
    [self.layer addSublayer:self.backgroundLayer];
    [self.layer addSublayer:self.animationLayer];
}

- (void)stopAnimation {
    _isPause = true;
    [self.backgroundLayer removeFromSuperlayer];
    [self.animationLayer removeFromSuperlayer];
    [self.layer removeAllAnimations];
    self.backgroundLayer = nil;
    self.animationLayer = nil;
}

- (void)pauseAnimation {
    if (_isPause == true) {
        return ;
    }
    _isPause = true;
    CFTimeInterval time = [self.animationLayer convertTime:CACurrentMediaTime() fromLayer:nil];
    self.animationLayer.speed = 0.0;
    self.animationLayer.timeOffset = time;
}

- (void)resumeAnimation {
    if (_isPause == false) {
        return ;
    }
    _isPause = false;
    CFTimeInterval pausedTime = self.animationLayer.timeOffset;
    self.animationLayer.speed = 1;
    self.animationLayer.timeOffset = 0;
    self.animationLayer.beginTime = 0;
    CFTimeInterval timeSincePause = [self.animationLayer convertTime:CACurrentMediaTime() fromLayer:nil] - pausedTime;
    self.animationLayer.beginTime = timeSincePause;
}


#pragma mark -----数据相关单元
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        // 初始化属性
        [self doInitializationData];
    }
    return self;
}

// 数据初始化
- (void)doInitializationData {
    _strokeBackgroundColor = [UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.5];
    _strokeColor = [UIColor whiteColor];
    _strokeWidth = 2.5;
    _strokeStart = 0.0;
    _strokeEnd = 0.5;
    _duration = 0.7;
}


#pragma mark - 界面相关单元
// 创建圆环
- (CAShapeLayer *)createShapeLayer:(CGFloat)strokeStart strokeEnd:(CGFloat)strokeEnd {
    CGFloat radius = (self.frame.size.height - self.strokeWidth) / 2.0;
    UIBezierPath *bezierPath = [UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width / 2.0, self.frame.size.height / 2.0) radius:radius startAngle:0 endAngle:2 * M_PI clockwise:YES];
    // 圆环遮罩
    CAShapeLayer *shapeLayer = [CAShapeLayer layer];
    shapeLayer.fillColor = [UIColor clearColor].CGColor;
    shapeLayer.strokeColor = [UIColor whiteColor].CGColor;
    shapeLayer.strokeStart = strokeStart;
    shapeLayer.strokeEnd = strokeEnd;
    shapeLayer.lineWidth = self.strokeWidth;
    shapeLayer.lineCap = kCALineCapRound;
    shapeLayer.lineDashPhase = 0.8;
    shapeLayer.path = bezierPath.CGPath;
    return shapeLayer;
}


#pragma mark - 懒加载单元
- (CALayer *)animationLayer {
    if (_animationLayer == nil) {
        CALayer *animationLayer = [CALayer layer];
        _animationLayer = animationLayer;
        animationLayer.frame = self.layer.bounds;
        animationLayer.backgroundColor = self.strokeColor.CGColor;
        animationLayer.mask = [self createShapeLayer:self.strokeStart strokeEnd:self.strokeEnd];
        [animationLayer addAnimation:self.rotationAnimation forKey:@"rotationAnimation"];
    }
    return _animationLayer;
}

- (CABasicAnimation *)rotationAnimation {
    CABasicAnimation *rotationAnimation;
    if (_rotationAnimation == nil) {
        rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        _rotationAnimation = rotationAnimation;
        rotationAnimation.fromValue = @(0);
        rotationAnimation.toValue = @(2 * M_PI);
        rotationAnimation.repeatCount = MAXFLOAT;
        rotationAnimation.duration = self.duration;
        rotationAnimation.removedOnCompletion = NO;
        rotationAnimation.fillMode = kCAFillModeForwards;
        rotationAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    }
    return _rotationAnimation;
}

- (CALayer *)backgroundLayer {
    if (_backgroundLayer == nil) {
        CALayer *backgroundLayer = [CALayer layer];
        _backgroundLayer = backgroundLayer;
        backgroundLayer.frame = self.layer.bounds;
        backgroundLayer.backgroundColor = self.strokeBackgroundColor.CGColor;
        backgroundLayer.mask = [self createShapeLayer:0.0 strokeEnd:1.0];
    }
    return _backgroundLayer;
}

- (void)dealloc {
    NSLog(@"%s", __func__);
}

@end
